import React from "react";
import ReactDom from "react-dom";
import "./kyc.css";
import filelogo from './nf/Path2254.png'

export function Kyc1(){
    return (
        <div className="container-fluid ">
            <div className="row">
                <div className="col ovalc">
                </div>
                <div className="col">
                    <h1 className="kyc"> <strong>KYC</strong> </h1>
                </div>            
            </div>
            <div className="row changehere changehereone">
                <div className="card outsidecard for2">
                    <div className="card-body text-left">
                        <h5 className="card-title">Upload Documents</h5>
                        <p className="card-text text-muted">Upload A Clear Image Of Your Chosen Personal Document To Each Category</p>
                        <div >
                            <div className="card insidecard zero">
                                <img src={filelogo} className="card-img-top filelogo new" />
                                <hr className="line"/>
                                <div className="card-body smallcard text-center">
                                    <h5 className="card-title ic">FRONT OF IC</h5>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Image is complete</h6>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Clearly visible</h6>
                                    <button type="button" className="btn btn-outline-primary btn-lg browse1">BROWSE</button>
                                    
                                </div>
                            </div>
                            <div className="card insidecard one three">
                                <img src={filelogo} className="card-img-top filelogo" />
                                <hr className="line"/>
                                <div className="card-body smallcard text-center">
                                    <h5 className="card-title ic">BACK OF IC</h5>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Image is complete</h6>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Clearly visible</h6>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Document in valid period</h6>

                                    <button type="button" className="btn btn-outline-primary btn-lg browse2">BROWSE</button>
                                </div>
                            </div>
                            <div className="card insidecard two four">
                                <img src={filelogo} className="card-img-top filelogo" />
                                <hr className="line"/>
                                <div className="card-body smallcard text-center">
                                    <h5 className="card-title ic">SELFIE WITH NOTE OF IC</h5>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Image is complete</h6>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Clearly visible</h6>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Document in valid period</h6>
                                    <h6 className="text-left"><i class="fas fa-check"></i>Note has today date</h6>
                                    <button  type="button" className="btn btn-outline-primary btn-lg browse3">BROWSE</button>
                                </div>
                            </div>
                            <button  type="button" className="btn btn-primary btn-xl lastbutton forthesecond">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}