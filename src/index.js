import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter as Router, Routes, Route, Redirect, Switch } from "react-router-dom"
import { Navbar } from "./navbar"
import { Landingpage } from "./landingpage"
import { Forgetpass } from "./forgetpass"
import { Kyc } from "./Kyc"
import { Kyc1 } from "./kyc1"
import Login from "./Login"
import {Signup} from "./signup"
import Email from "./email"
import {Password} from "./password"
import Kform from "./form"
function Atf(){
  return(
    <div className = 'container-fluid'>
      <Router>
        <Switch>
          <div className='row ats'>
            <Navbar />
            <Route path="/landing" component = {Landingpage}/>
            <Route path="/login" component={Login} />
            <Route path='/forgetp' component= {Forgetpass} />
            <Route path='/kyc' component= {Kyc} />
            <Route path='/kyc1' component= {Kyc1} />
            <Route path='/signup' component={Signup} />
            <Route path='/email' component={Email} />
            <Route path='/password' component={Password}/>
            <Route path='/form' component= {Kform} />
            <Redirect to='landing' />
          </div>
        </Switch>
      </Router>
    </div>
  );
}
ReactDOM.render(<Atf />, document.getElementById('root'));