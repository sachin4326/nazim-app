import ReactDom from "react-dom";
import React from "react";
import './fp.css';
import forgetlogo from './nf/Group830.png';
import { TextField } from "@mui/material";
import { Link } from "react-router-dom";

export function Password(){
    return(
        <div className="container-fluid"> 
            <div className="row firstrow">
                <div className="col ads">
                    
                </div>
                <div className="col">
                    <h1 className="awd"><span className="fw-light">CREATE</span> <br /><strong> NEW PASSWORD</strong> </h1>
                </div>
            </div>
            <div className="row secondrow">
                <img src={forgetlogo} className="forgetlogo"/>
            </div>
            <div className="row thirdrow">
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title">Create new password</h5>
                        <h6 class="card-subtitle">Your new password must be different from previous used password</h6>
                        <TextField label="Password" color="secondary" className="textf"/>
                        <TextField label="check password" color="secondary" className="textf"/>
                        <br />
                        <button type="button" class="btn btn-lg rounded-pill si"><Link to="/login">Reset Password</Link></button>
                    </div>
                </div>
            </div>
        </div>
    );
}