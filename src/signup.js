import ReactDom from "react-dom";
import React from "react";
import './fp.css';
import forgetlogo from './nf/Group 521.png';
import { TextField } from "@mui/material";
import { Link } from "react-router-dom";

export function Signup(){
    return(
        <div className="container-fluid"> 
            <div className="row firstrow">
                <div className="col ads">
                    
                </div>
                <div className="col">
                    <h1 className="awd"><span className="fw-light">FIRST TIME</span> <br /><strong> THESE DETAILS</strong><br />ARE REQUIRED </h1>
                </div>
            </div>
            <div className="row secondrow">
                <img src={forgetlogo} className="forgetlogo"/>
            </div>
            <div className="row thirdrow">
                <div class="card text-center">
                    <div class="card-body text-dark">
                        <h5 class="card-title text-dark">Introduce Yourself</h5>
                        <h6 class="card-subtitle text-dark">We keep this information private</h6>
                        <TextField label="Full Name" color="secondary" className="textf"/>
                        <br />
                        <TextField label="Email Address" color="secondary" className="textf"/>
                        <br />
                        <TextField label="Password" color="secondary" className="textf"/>
                        <br />
                        <button type="button" class="btn btn-lg rounded-pill si"><Link to="/login">Submit</Link></button>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Signup
