import React from "react";
import ReactDom from "react-dom";
import companyLogo from "./nf/Group796.png";
import "./fd.css";
import { shadows, Box } from "@mui/system";
import moneyg from "./nf/6027540.png";
import dollar from "./nf/1.png";

export function Landingpage() {
  return (
    <div>
      <div className="container-fluid fc">
        <div className="row ">
          <div className="col-6 wel fw-bold">
            <div className="row">
              <p className="wl">Welcome</p>
            </div>
            <div className="row">
              <h1 className="cf">The Internet of Blockchains</h1>
            </div>
            <div className="row">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s
              </p>
            </div>
          </div>
          <div className="col-3">
            <img className="bitcoin" src={companyLogo} />
          </div>
        </div>
      </div>
      <div className="container-fluid ths">
        <div className="row sc">
          <div className="col-5 ok">
            <div className="row ">
              <h3 className="tsize">LOREM IPSUM</h3>
              <p className="tsizet">
                MAURIS IMPERDIET ORCI DAPIBUS, COMMODO LIBERO NEC, INTERDUM
                TORTOR.
              </p>
            </div>
          </div>
          <div className="col-5 shaw this">
            <p>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using 'Content here,
              content here', making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for 'lorem ipsum' will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </p>
          </div>
        </div>
        <div className="row cm">
          <div className="col text-center">
            <h5 className="CM">CRYPTOCURRENCY MARKET</h5>
            <br />
            <br />
            <h2 className="tfof">THE FUTURE OF MONEY</h2>
            <p className="rlf">
              Duis Porta, Ligula Rhoncus Euismod Pretium, Nisi Tellus Eleifend
              Odio, Luctus Viverra Sem Dolor Id Sem. Maecenas A Venenatis Enim,
              Quis Porttitor Magna. Etiam Nec Rhoncus Neque. Sed Quis .
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-auto">
            <img src={moneyg} className="moneyg" />
          </div>
          <div className="col-auto">
            <h2 className="buycpy fw-bold">Buy crypto at true cost</h2>

            <img src={dollar} className="dollar" />
            <br />
            <br />
            <button type="button" className="btn btn-primary ml-5 mt-1">
              Learn More
            </button>

            <button type="button" className="btn btn-primary ml-5 mt-1">
              Buy Now
            </button>
            <h3 className="mt-5 fw-normal w-75">
              SECURELY BUY, SELL, STORE, SEND and TRACK
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
}
