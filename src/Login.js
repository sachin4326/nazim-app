import React from 'react'
import "./Login.css"
import img from './img/5163951.png'

import { Link } from 'react-router-dom';
// import ReactDOM from 'react-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import {Typography} from '@mui/material';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { width } from '@mui/system';

const validationSchema = yup.object({
  email: yup
    .string('Enter your email')
    .email('Enter a valid email')
    .required('Email is required'),
  password: yup
    .string('Enter your password')
    .min(8, 'Password should be of minimum 8 characters length')
    .required('Password is required'),
});

function Login() {
    const formik = useFormik({
        initialValues: {
          email: '',
          password: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
          alert(JSON.stringify(values, null, 2));
        },
      });
    
    return (
        <div className="container">
            <div className="col1 circle"></div>
            <section>
              <h2 classname='para' style={{color:'white',fontSize:'35px', fontFamily:'Poppins', width:"500px"}}>WELCOME</h2>
              <h1 className='para1'style={{color:'white',fontSize:'55px',marginBlock:'auto',marginTop:'-15px', fontFamily:'Poppins', width:"500px"}}>PLEASE LOGIN</h1>
              <h2 className='para2'style={{color:'white',fontSize:'25px',marginBlock:'auto', fontFamily:'Poppins',  width:"500px"}}>TO CONTINUE!</h2>
              <img style={{height:"500px",marginTop:"100px",marginLeft:"30px",width:"500px"}} src={img}/>
            </section>
            <div className="col2" >
              {/* <Card id="card" > */}
              <form onSubmit={formik.handleSubmit}>
               <Box sx={{textAlign:'center'}}>
                  <Typography variant='h5' className="textSign" style={{fontFamily:'Poppins',color:'black'}}><h5>Sign In to your account</h5></Typography>
                  <Typography body='2' className="textSign"  style={{fontFamily:'Poppins',color:'black'}}>Dont have an account.Create your account,</Typography>
                  <Typography body='2' className="textSign" style={{fontFamily:'Poppins',color:'black'}} >it takes less than a minute</Typography>   
               </Box>
                <Box sx={{ width: '500px', maxWidth: '100%', padding:'15px',}} className="input">
                    <TextField fullWidth label="Email Address" id="emailaddress" />

                </Box>
                <Box sx={{ width: '500px', maxWidth: '100%', padding:'15px',}} className="input">
                    <TextField
                    fullWidth
                    id="password"
                    name="password"
                    label="Password"
                    type="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                    />
                </Box>
                  <Box sx={{marginLeft:'350px'}} className="input">
                     <Link to='forgetp'><a className="text-muted" style={{textDecoration:'none'}} className="fp">Forget Password?</a></Link>
                  </Box>
                <Box sx={{display:'flex',justifyContent:'space-between' ,padding:'15px'}} className="input">

                  <Link to='/form'><Button color="primary" variant="contained" type="submit" className="Logbuttn">
                  Login
                  </Button></Link>
                  
                  <Typography body='2' className="text-muted" style={{fontFamily:'Poppins', color:'Black'}} className="fp">Don't have an account? <Link to='/signup'><a className="SignButton">Sign Up</a></Link></Typography> 
                </Box>
                <Box sx={{display:'flex',justifyContent:'center' ,padding:'15px' }}> 
                <Typography body='2' className="text-muted"  > Or connect using </Typography>   
                </Box>
                
                <Box sx={{display:'flex',justifyContent:'space-evenly'}}>
                <Card style={{width:'150px'}}>
                
                <Box sx={{display:'flex',justifyContent:'space-evenly'}}>
                <i class="fab fa-facebook"></i>
                <Typography body='2' className="text-muted"  >Facebook</Typography> 
                </Box>
                </Card>
                <Card style={{width:'150px'}}>
                
                <Box sx={{display:'flex',justifyContent:'space-evenly' , boxShadow:"0px 2px 5px", opacity:"1"}}>
                <i class="fab fa-google"></i>
                <Typography body='2' className="text-muted"  >Google</Typography> 
                </Box>
                </Card>
                </Box>
              </form>
              {/* </Card> */}
                
            </div>
        </div>
    )
}

export default Login
// ReactDOM.render(<WithMaterialUI />, document.getElementById('root'));
