import ReactDom from "react-dom";
import React from "react";
import './fp.css';
import forgetlogo from './nf/Group 521.png';
import { TextField } from "@mui/material";
import { Link } from "react-router-dom";

export function Email(){
    return(
        <div className="container-fluid"> 
            <div className="row firstrow">
                <div className="col ads">
                    
                </div>
                <div className="col">
                    <h1 className="awd"><span className="fw-light">FIRST TIME</span> <br /><strong> THESE DETAILS</strong><br />ARE REQUIRED </h1>
                </div>
            </div>
            <div className="row secondrow">
                <img src={forgetlogo} className="forgetlogo"/>
            </div>
            <div className="row thirdrow">
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title text-dark">Check Your Email</h5>
                        <h6 class="card-subtitle text-dark">We send you Password recover instructions to your email</h6>
                        <button type="button" class="btn btn-lg rounded-pill si"><Link to="/password">Submit</Link></button>
                        <br />
                        <br />
                        <h6 class="card-subtitle text-dark">Did not receive email ? check Spam filter or<Link to='/forgetp'> try another email id</Link> </h6>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Email;
