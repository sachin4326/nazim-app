import ReactDom from "react-dom";
import React from "react";
import './fp.css';
import forgetlogo from './nf/Group830.png';
import { TextField } from "@mui/material";
import { Link } from "react-router-dom";
export function Forgetpass(){
    return(
        <div className="container-fluid"> 
            <div className="row firstrow">
                <div className="col ads">
                    
                </div>
                <div className="col">
                    <h1 className="awd"><span className="fw-light">FORGET</span> <br /><strong> PASSWORD</strong> </h1>
                </div>
            </div>
            <div className="row secondrow">
                <img src={forgetlogo} className="forgetlogo"/>
            </div>
            <div className="row thirdrow">
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title text-dark">Forgot Password</h5>
                        <h6 class="card-subtitle text-dark">Enter the email associated with your account and we'll send an email with instructions to reset your password</h6>
                        <TextField label="Email Address" color="secondary" className="textf"/>
                        <br />
                        <button type="button" class="btn btn-lg rounded-pill si"><Link to="/email">Send Instructions</Link></button>
                    </div>
                </div>
            </div>
        </div>
    );
}