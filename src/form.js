import React from "react";
import { useState } from "react";
import "./form.css";
import { Formik } from "formik";
import {
  Button,
  Form,
  InputGroup,
  Row,
  Col,
  FormGroup,
  FormControl,
  ControlLabel,
} from "react-bootstrap";
import * as yup from "yup";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";
import { dividerClasses, Typography } from "@mui/material";
// import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { Link } from "react-router-dom";
// const validationSchema = yup.object({
//   email: yup
//     .string("Enter your email")
//     .email("Enter a valid email")
//     .required("Email is required"),
//   password: yup
//     .string("Enter your password")
//     .min(8, "Password should be of minimum 8 characters length")
//     .required("Password is required"),
// });

// function Kyc() {
//   const formik = useFormik({
//     initialValues: {
//       FullName: "",
//       email: "",
//     },
//     validationSchema: validationSchema,
//     onSubmit: (values) => {
//       alert(JSON.stringify(values, null, 2));
//     },
//   });

// Example starter JavaScript for disabling form submissions if there are invalid fields
//   (function () {
//     "use strict";
//     window.addEventListener(
//       "load",
//       function () {
//         // Fetch all the forms we want to apply custom Bootstrap validation styles to
//         var forms = document.getElementsByClassName("needs-validation");
//         // Loop over them and prevent submission
//         var validation = Array.prototype.filter.call(forms, function (form) {
//           form.addEventListener(
//             "submit",
//             function (event) {
//               if (form.checkValidity() === false) {
//                 event.preventDefault();
//                 event.stopPropagation();
//               }
//               form.classList.add("was-validated");
//             },
//             false
//           );
//         });
//       },
//       false
//     );
//   })();
//   return (
//     <div className="container">
//       <div className="col1">
//         <section>
//           <h1
//             className="para"
//             style={{
//               color: "white",
//               fontSize: "65px",
//               marginLeft: "-5px",
//               fontWeight: "900",
//               fontFamily: "Poppins",
//               marginTop: "35px",
//             }}
//           >
//             KYC
//           </h1>
//         </section>
//       </div>
//       <div className="col2">
//         {/* <Card className="card">  */}
//         <form onSubmit={formik.handleSubmit}>
//           <Box sx={{ marginLeft: "-80px" }}>
//             <Typography
//               variant="h5"
//               className="textSign"
//               style={{ fontFamily: "Poppins", color: "#3634E0" }}
//             >
//               <h5>Sign In to your account</h5>
//             </Typography>
//             <Typography
//               body="2"
//               className="textSign"
//               style={{ fontFamily: "Poppins" }}
//             >
//               Dont have an account.Create your account,
//             </Typography>
//             <Typography
//               body="2"
//               className="textSign"
//               style={{ fontFamily: "Poppins" }}
//             >
//               it takes less than a minute
//             </Typography>
//           </Box>
//           <Box sx={{ width: "500px", maxWidth: "100%", padding: "15px" }}>
//             <TextField fullWidth label="Email Address" id="emailaddress" />
//           </Box>
//           <Box sx={{ width: 500, maxWidth: "100%", padding: "15px" }}>
//             <TextField
//               fullWidth
//               id="password"
//               name="password"
//               label="Password"
//               type="password"
//               value={formik.values.password}
//               onChange={formik.handleChange}
//               error={formik.touched.password && Boolean(formik.errors.password)}
//               helperText={formik.touched.password && formik.errors.password}
//             />
//           </Box>
//           <Box sx={{ marginLeft: "350px" }}>
//             <a
//               className="text-muted"
//               style={{ textDecoration: "none" }}
//               href="#"
//             >
//               Forget Password?
//             </a>
//           </Box>
//           <Box
//             sx={{
//               display: "flex",
//               justifyContent: "space-between",
//               padding: "15px",
//             }}
//           >
//             <Button
//               color="primary"
//               variant="contained"
//               type="submit"
//               className="Logbuttn"
//             >
//               Login
//             </Button>

//             <Typography
//               body="2"
//               className="text-muted"
//               style={{ fontFamily: "Poppins" }}
//             >
//               Don't have an account?{" "}
//               <a className="SignButton" href="#">
//                 Sign Up
//               </a>
//             </Typography>
//           </Box>
//         </form>
//         {/* </Card> */}
//       </div>
//     </div>
//   );
// }

// const { Formik } = formik;

function Kform() {
  const schema = yup.object().shape({
    firstName: yup.string().required(),
    lastName: yup.string().required(),
    email: yup.string().required(),
    phone: yup.string().required(),
    dateofbirth: yup.string().required(),
    gender: yup.string().required(),
    address: yup.string().required(),
    city: yup.string().required(),
    state: yup.string().required(),
    postal: yup.string().required(),
    file: yup.mixed().required(),
    terms: yup.bool().required().oneOf([true], "terms must be accepted"),
  });
  return (
    <div className="container">
      <div className="col1">
        <section>
          <h1
            className="para"
            style={{
              color: "white",
              fontSize: "65px",
              marginLeft: "-5px",
              fontWeight: "900",
              fontFamily: "Poppins",
              marginTop: "35px",
            }}
          >
            KYC
          </h1>
        </section>
      </div>
      <div className="col2">
        <div className="form">
          <Formik
            validationSchema={schema}
            onSubmit={console.log}
            initialValues={{
              firstName: "",
              lastName: "",
              email: "",
              phone: "",
              address:"",
              city: "",
              state: "",
              postal: "",
              file: null,
              terms: false,
            }}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              isValid,
              errors,
            }) => (
                <Box >
                  
                  <Typography
                    variant="h5"
                    style={{
                      marginTop: "-40px",
                      marginLeft:"-4px",
                      fontFamily: "Poppins",
                      color: "#3634E0",

                    }}
                  >
                    
                    Personal Details
                    <Typography body="2" className="text-muted">
                    Enter Your Details As They Appear on Your Documentation
                  </Typography>
                  </Typography>

                  
                <Box style={{marginTop:'50px'}}>
                  <Form noValidate onSubmit={handleSubmit}>
                    <Row className="mb-3">
                      <Form.Group
                        as={Col}
                        md="4"
                        controlId="validationFormik101"
                        className="position-relative"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>First name</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="John"

                          name="firstName"
                          value={values.firstName}
                          onChange={handleChange}
                          isValid={touched.firstName && !errors.firstName}
                        />
                        <Form.Control.Feedback tooltip>
                          Looks good!
                        </Form.Control.Feedback>
                      </Form.Group>
                      <Form.Group
                        as={Col}
                        md="4"
                        controlId="validationFormik102"
                        className="position-relative"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Last name</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Doe"

                          name="lastName"
                          value={values.lastName}
                          onChange={handleChange}
                          isValid={touched.lastName && !errors.lastName}
                        />

                        <Form.Control.Feedback tooltip>
                          Looks good!
                        </Form.Control.Feedback>
                      </Form.Group>
                      <Form.Group
                        as={Col}
                        md="4"
                        controlId="validationFormikUsername2"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Email address</Form.Label>
                        <InputGroup hasValidation>
                          <InputGroup.Text id="inputGroupPrepend">
                            @
                          </InputGroup.Text>
                          
                          <Form.Control
                            type="text"
                            placeholder="johndoe@gmail.com"
                            aria-describedby="inputGroupPrepend"
                            name="Email address"
                            value={values.email}
                            onChange={handleChange}
                            isInvalid={!!errors.email}
                          />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.email}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>
                    </Row>
                    <Row className="mb-3">
                    <Form.Group
                        as={Col}
                        md="3"
                        controlId="validationFormikUsername2"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Phone</Form.Label>
                        <InputGroup hasValidation>
                          <InputGroup.Text id="inputGroupPrepend">
                            +91
                          </InputGroup.Text>
                          
                          <Form.Control
                            type="text"
                            placeholder="9876543210"
                            aria-describedby="inputGroupPrepend"
                            name="phone"
                            value={values.phone}
                            onChange={handleChange}
                            isInvalid={!!errors.phone}
                          />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.phone}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>
                      
                      <Form.Group
                        as={Col}
                        md="3"
                        controlId="validationFormikUsername2"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Date of Birth</Form.Label>
                        <InputGroup hasValidation>
                          
                          <Form.Control
                            type="text"
                            placeholder="9876543210"
                            aria-describedby="inputGroupPrepend"
                            name="phone"
                            value={values.phone}
                            onChange={handleChange}
                            isInvalid={!!errors.phone}
                          />
                          <InputGroup.Text id="inputGroupPrepend">
                          <i class="fas fa-calendar-alt"></i>
                          </InputGroup.Text>
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.phone}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>
                        
                      <Form.Group as={Col} controlId="formGridState">
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Gender</Form.Label>
                        <Form.Select defaultValue="Choose...">
                          <option>Gender</option>
                          <option>...</option>
                        </Form.Select>
                      </Form.Group>
                      <Form.Group
                        as={Col}
                        md="3"
                        controlId="validationFormikUsername2"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Address</Form.Label>
                        <InputGroup hasValidation>
                          
                          <Form.Control
                            type="text"
                            placeholder="Address"
                            aria-describedby="inputGroupPrepend"
                            name="address"
                            value={values.address}
                            onChange={handleChange}
                            isInvalid={!!errors.address}
                          />
                          <Form.Control.Feedback type="invalid" tooltip>
                            {errors.address}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Group>

                        
                        
                    </Row>
                    <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridState">
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>State</Form.Label>
                        <Form.Select defaultValue="Choose...">
                          <option>Choose State</option>
                          <option>...</option>
                        </Form.Select>
                      </Form.Group>
                      <Form.Group as={Col} controlId="formGridState">
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>City</Form.Label>
                        <Form.Select defaultValue="Choose...">
                          <option>Choose City</option>
                          <option>...</option>
                        </Form.Select>
                      </Form.Group>

                      <Form.Group as={Col} controlId="formGridState">
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Country</Form.Label>
                        <Form.Select defaultValue="Choose...">
                          <option>Choose Country</option>
                          <option>...</option>
                        </Form.Select>
                      </Form.Group>
                      
                      <Form.Group
                        as={Col}
                        md="3"
                        controlId="validationFormik105"
                        className="position-relative"
                      >
                        <Form.Label style={{fontFamily:'Poppins',color:"#3634E09C"}}>Postal</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="123456 "
                          name="zip"
                          value={values.zip}
                          onChange={handleChange}
                          isInvalid={!!errors.zip}
                        />

                        <Form.Control.Feedback type="invalid" tooltip>
                          {errors.zip}
                        </Form.Control.Feedback>
                      </Form.Group>
                    </Row>
                    
                   <Link to ="/kyc"><Button type="submit" className='SubButtn'>Submit</Button></Link>
                  </Form>
                </Box>
                </Box>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default Kform;
