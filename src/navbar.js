import react from "react";
import reactDom from "react-dom";
import './naco.css';
import { Link } from "react-router-dom";

export function Navbar(){
    return (
    <nav className="navbar navbar-expand-sm navbar-dark">
        <div className="container-fluid">
            <a className="navbar-brand"><Link to="/landing">Logo</Link></a>
            <button className="navbar-toggler mr-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav mydiv" >
                    <li className="nav-item">
                        <a className="nav-link" href="#">Exchange</a>
                    </li>
                    <li className="nav-item">
                        <a class="nav-link" href="#">Bridge</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link " href="#">Contact Us</a>
                    </li>
                    <li className="nav-item">
                        <Link to="/login"><button className="btn btn-primary"><a className="nav-link"> Log In</a></button></Link>
                    </li>
                    <li className="nav-item">
                        <Link to='/signup'><button className="btn btn-primary"><a className="nav-link">Sign Up</a></button></Link>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    );
}