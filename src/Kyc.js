import React from "react";
import ReactDom from "react-dom";
import "./kyc.css";
import filelogo from './nf/Icon ionic-md-document.png'
import { Link } from "react-router-dom";

export function Kyc(){
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col ovalc">
                </div>
                <div className="col">
                    <h1 className="kyc"> <strong>KYC</strong> </h1>
                </div>            
            </div>
            <div className="row changehere">
                <div className="card outsidecard ">
                    <div className="card-body text-left">
                        <h5 className="card-title">Upload Documents</h5>
                        <p className="card-text text-muted">Upload A Clear Image Of Your Chosen Personal Document To Each Category</p>
                        <div>
                            <div className="card insidecard zero">
                                <img src={filelogo} className="card-img-top filelogo" />
                                <hr className="line"/>
                                <div className="card-body smallcard text-center">
                                    <h5 className="card-title ic">FRONT OF IC</h5>
                                    <button type="button" className="btn btn-outline-primary btn-lg">UPLOAD</button>
                                </div>
                            </div>
                            <div className="card insidecard one">
                                <img src={filelogo} className="card-img-top filelogo" />
                                <hr className="line"/>
                                <div className="card-body smallcard text-center">
                                    <h5 className="card-title ic">BACK OF IC</h5>
                                    <button type="button" className="btn btn-outline-primary btn-lg">UPLOAD</button>
                                </div>
                            </div>
                            <div className="card insidecard two">
                                <img src={filelogo} className="card-img-top filelogo" />
                                <hr className="line"/>
                                <div className="card-body smallcard text-center">
                                    <h5 className="card-title ic">SELFIE WITH NOTE OF IC</h5>
                                    <button  type="button" className="btn btn-outline-primary btn-lg ">UPLOAD</button>
                                </div>
                            </div>
                            <Link to='/kyc1'><button  type="button" className="btn btn-primary btn-xl lastbutton">SUBMIT</button></Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}